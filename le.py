import csv
dados1 = open('dados1.csv')
lerArquivo = csv.reader(dados1, delimiter=";")
lista = list(lerArquivo)
tratados = open('tratado.csv', 'w')
tratados.write("nome;peso;altura;imc\n")
for linha in lista:
    nome = linha[0]
    peso = int(linha[1])
    altura = int(linha[2])/100
    imc = float(peso/(altura*altura))
    imc = '{:.2f}'.format(imc)
    if peso > 99:
        linha = nome+";"+str(peso)+";"+str(altura)+";"+imc+"\n"
        tratados.write(linha)
dados1.close()
tratados.close()
